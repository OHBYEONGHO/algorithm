package _2_Fibonacci;

public class Main {
	public static void main(String[] args) {


		int[] arr = new int[100];
		
		arr[1] = 1;
		arr[2] = 1;
		
		for(int i=3; i<100; i++) {
			arr[i] = arr[i-1] +arr[i-2];
		}
		
		for(int i=1; i<=10; i++) {
			System.out.print(arr[i] + " ");
		}
		System.out.println();
		int pre = 1;
		int ppre = 1;
		int num =0;
		System.out.print(pre + " ");
		System.out.print(ppre + " ");
		for(int i=0; i<8; i++) {
			num = pre + ppre;
			ppre = pre;
			pre = num; 
			System.out.print(num + " ");
		}
		
	}
}
