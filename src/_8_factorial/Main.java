package _8_factorial;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		System.out.println("숫자를 입력하시오");
		
		int input = scan.nextInt();
		int answer = 1;
		
		for(int i=1; i<=input;i++) {
			answer *= i;
		}
		
		System.out.println("answer== " + answer);
	}

}
