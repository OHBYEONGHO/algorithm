package _13_star;

public class Star {

	public static void main(String[] args) {
		int input = 5;
		
		for(int i =1; i<=input; i++) {
			for(int z=1; z<=i;z++) {
				System.out.print("*");
			}
			System.out.println();
		}
		
		System.out.println("/////////////////////////////");
		System.out.println("/////////////////////////////");
		System.out.println("/////////////////////////////");
	}

}
