package _9_sum;

public class Main {
	public static void main(String[] args) {
		int input = 12321;
		int answer = 0;
		
		while(input > 0) {
			answer += input % 10;
			input /= 10;
		}
		
		System.out.println("answer==" + answer);
	}
}
