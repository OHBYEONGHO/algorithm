package _6_gcd;

public class Main {

	public static void main(String[] args) {
		int num1 = 12;
		int num2 = 22;
		int gcd = 1;
		int big;
		int small;
		
		if(num1 > num2) {
			big = num1;
			small = num2;
		} else {
			big = num2;
			small = num1;
		}
		
		
		for(int i=1; i<=small; i++) {
			if(big % i == 0 && small % i == 0) {
				gcd = i;
			}
		}
		System.out.println(gcd);
	}

}
