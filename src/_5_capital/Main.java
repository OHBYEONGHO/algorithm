package _5_capital;

public class Main {

	public static void main(String[] args) {
		
		String word = "HellowORLD";
		
		char[] arr = word.toCharArray();
		
		for(int i=0;i<arr.length;i++) {
			if(arr[i] >= 'a' && arr[i] <= 'z') {
				arr[i] = (char) (arr[i] - 32);
			} else {
				arr[i] = (char) (arr[i] + 32);
			}
			System.out.print(arr[i]);
		}
	}

}
