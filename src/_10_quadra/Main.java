package _10_quadra;

public class Main {

	public static void main(String[] args) {
		int input = 4;

		for(int i = 0; i<input;i++) {
			for(int z = 1; z<=input; z++) {
				System.out.printf("%5d", z + input*i);
			}
			System.out.println();
		}
		
		System.out.println("////////////////////////////////////");
		// i = 1 -> input * 2
		// i = 3 => input * 4
		
		for(int i = 0; i<input;i++) {
			for(int z = 1; z<=input; z++) {
				if(i % 2 == 0) {
					System.out.printf("%5d", z + input*i);
				} else {
					System.out.printf("%5d", input*(i+1)-z+1);
				}
			}
			System.out.println();
		}
		
		System.out.println("////////////////////////////////////");
		
		for(int i=1; i<=input; i ++) {
			for (int z=0; z<input; z++) {
				System.out.printf("%5d", i + input*z);
			}
			System.out.println();
		}
		
		System.out.println("////////////////////////////////////");
		
		int[][] arr = new int[input][input];
		int num = 1;
		for(int i = 0; i<input;i++) {
			for(int z = 0; z<input; z++) {
				arr[z][i] = num;
				num++;
			}
			System.out.println();
		}
		
		for(int i = 0; i<input;i++) {
			for(int z = 0; z<input; z++) {
				System.out.printf("%5d", arr[i][z]);
			}
			System.out.println();
		}
		
		System.out.println("////////////////////////////////////");
		// 00 1  01 2 02 3
		// 10 2  11 4 12 6
		// 20 3  21 6 22 9
		// 30 4  31 8 32 12
		
		arr = new int[input][input];
		num = 1;
		for(int i = 0; i<input;i++) {
			for(int z = 0; z<input; z++) {
				arr[i][z] = (i+1) * (z+1);
			}
			System.out.println();
		}
		for(int i = 0; i<input;i++) {
			for(int z = 0; z<input; z++) {
				System.out.printf("%5d", arr[i][z]);
			}
			System.out.println();
		}
	}

}
