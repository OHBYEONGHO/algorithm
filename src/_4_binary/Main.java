package _4_binary;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("숫자를 입력하시오");
		int num = scan.nextInt();
		int[] result = new int[10];
		boolean flag = true;
		int i = 0;
		
		
		
		while(flag) {
			result[i] = num % 2;
			num = num/2;
			i++;
			if(num == 0) {
				flag = false;
			}
		}
		
		i--;
		
		for(; i>=0; i--) {
			System.out.print(result[i] + " ");
		}
		
	}

}
