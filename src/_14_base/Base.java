package _14_base;

public class Base {

	public static void main(String[] args) {
		int base = 3;
		int input = 5;
		int ref = 1;
		for(int i =0; i<base; i++) {
			ref *= input;
		}
		
		System.out.println(ref);
	}
}
