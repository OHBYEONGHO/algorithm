package _1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Student s1 = new Student("손오공", "1111");
		Student s2 = new Student("저팔계", "2222");
		Student s3 = new Student("사오정", "3333");
		Scanner scan = new Scanner(System.in);
		List<Student> list = new ArrayList<>();
		
		boolean flag = false;
		
		list.add(s1);
		list.add(s2);
		list.add(s3);
		
		while(true) {
			System.out.println("계속할거면 y, 그만 n");
			String input = scan.next();
			if(input.equals("y")) {
				System.out.println("학생 이름은?");
				String name = scan.next();
				for(Student std : list) {
					if(std.getName().equals(name)) {
						System.out.println("학생 번호는 == "+std.getNo());
						flag = true;
					} 
				}
				
				if(!flag) {
					System.out.println("그런애없어");
				}
			} else if(input.equals("n")) {
				System.out.println("겜끝!");
				break;
			}
		}
		
		
	}
}
